#installation
#node

curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -

sudo apt install nodejs

node -v

npm -v

#react

sudo npm install -g create-react-app

#install all dependencies

npm install

#start project

npm start