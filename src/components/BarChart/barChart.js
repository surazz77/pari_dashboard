import React ,{Component} from 'react'
import {Bar } from 'react-chartjs-2'
class BarChart extends Component {
	state={
		labels: [],
	}

	componentDidMount=()=> {
		const {labels} = this.props
		this.setState({labels})
	}
	render() {
		const {labels} = this.state
		console.log(labels)

		const chartdata = {
			width:'100px',
			labels: labels,
			datasets:[{
				data:[
					500,800,700,900,1000,600,450
					],
					backgroundColor:[
					'rgba(255, 0, 0, 0.2)',
					'rgba(255, 0, 0, 0.4)',
					'rgba(255, 0, 0, 0.6)',
					'rgba(255, 0, 0, 0.8)',
					'rgba(155, 0, 0, 0.8)',
					'rgba(100, 0, 0, 0.2)',
					'rgba(350, 0, 0, 0.8)',
				]
			}]
		
	
		}
			
		return (
		<div>
			{
				((labels && labels.length) > 0) && 
					<Bar
						data={chartdata}
						width={100}
						height={20}
						options={{
							legend:{
								display:false
							},
							scales:{
								yAxes: [{
									
								}],
								xAxes: [{ 
									barPercentage: 1.0,
									categoryPercentage: 0.5,
									categorySpacing: 1
								}],
						
							}
						}}
					/>
			}
			

		</div>
		);
	}
}
export default BarChart;                              