import React from 'react'
import BarChart from '../BarChart/barChart'
import '../../App.css'
const labels = [
    '1pm','2pm','3pm','4pm','5pm','6pm','7pm'
]
const labels1 = [
    '1pm','2pm','3pm','4pm','5pm','6pm','7pm'
]
const LogInfo=()=>(
    <div className='logInfo'>
        <div>
            <h1>Movement</h1>
            <div className="card">
                <div className="container">
                    <BarChart labels={labels}/>                
                </div>
            </div> 
            
           
        </div>
        <div>
        <h1>Peak Time</h1>
        <div className="card">
                <div className="container">
                    <BarChart labels={labels}/>                
                </div>
            </div> 
        </div>
        
        </div>
)
export default LogInfo