import React,{Component} from 'react'
import SideBar from '../SideBar/sidebar'
import TopIcon from '../TopIcon/topIcon'
import NavBar from '../TopNav/topnav'
import LogInfo from '../LogInfo/logInfo'
import '../../App.css'
class Logs extends Component {
	constructor(props) {
		super(props)

		this.state = {
			showSideBar: true,
		}
	}

	toggleSidebar = () => {
		this.setState(({showSideBar}) => ({
			showSideBar: !showSideBar
		}))
	}

	render(){  	
	const {showSideBar} = this.state
	return (
		<div className="container">
			{showSideBar?
			<div className={showSideBar ? 'side': 'side1'}>
				{console.log(this.state.showSideBar)}
				< SideBar toogleSidebar={this.toggleSidebar}/>
			</div>:<div>
					<i className="fa fa-bars" onClick={this.toggleSidebar}></i>
					</div>}
			

			<div className={showSideBar ? 'content': 'content1'}>
				
				<div>
					<TopIcon/><br/><br/>
				</div>				
				<div>
					<NavBar/>
				</div>
				<div>
					<LogInfo/>
				</div>
				
			
			</div>
		</div>
	)
}
}
export default Logs