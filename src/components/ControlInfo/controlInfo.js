import React,{Component} from 'react'
import '../../App.css'
class ControlInfo extends Component{
    state={
        status:false
    }
    toogle=()=>{
        this.setState(({status})=>({
            status:!status

        }))
    }

    render(){
        console.log(this.state.status)
        return(
            <div className='controlInfo'>
        <h1>Controls  </h1>
        <div className='card'>
        <div className='container'>
            <div className='controller'>

                    <div><b>Active</b></div>
                    <div>
                        <label className="switch">
                            <input type="checkbox" onChange={this.toogle}/>
                            <span className="slider round"></span>
                        </label>
                    </div>

            </div>
            
            <div className='controller'>
                    <div><b>Camera</b></div>
                    <div>
                        <label className="switch">
                            <input type="checkbox"/>
                            <span className="slider round"></span>
                        </label>
                    </div>
            </div>
            
            <div className='controller'>
                    <div><b>Voice</b></div>
                    <div>
                    <label className="switch">
                        <input type="checkbox"/>
                        <span className="slider round"></span>
                    </label>
                    </div>
            </div>
        </div>
        </div>
        </div>

        )
    }
}
    

export default ControlInfo