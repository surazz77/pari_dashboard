import React,{Component} from 'react'
import SideBar from '../SideBar/sidebar'
import TopIcon from '../TopIcon/topIcon'
import NavBar from '../TopNav/topnav'
import '../../App.css'
class Home extends Component {
	state={
		showSideBar:true
	}

	toggleSidebar = () => {
		this.setState(({showSideBar}) => ({
			showSideBar: !showSideBar
		}))
	}

	render(){  	
	return (
		<div className="container">
			{this.state.showSideBar?
			<div className={this.state.showSideBar ? 'side': 'side1'}>
				{console.log(this.state.showSideBar)}
				< SideBar toogleSidebar={this.toggleSidebar}/>
			</div>:<div>
					<i className="fa fa-bars" onClick={this.toggleSidebar}></i>
					</div>}
			

			<div className={this.state.showSideBar ? 'content': 'content1'}>
				
				<div>
					<TopIcon/><br/><br/>
				</div>				
				<div>
					<NavBar/>
				</div>
			
			</div>
		</div>
	)
}
}
export default Home