import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import '../../App.css';
class Sidebars extends Component {
	// state={
	// 	isCollapsed:false
	// }
	// toggleSidebar=()=>{
	// 	this.setState({
	// 		isCollapsed=!isCollapsed
	// 	})
	// }
	// componentDidMount=()=>{
	// 	if (this.state.isCollapsed){
			
	// 	}

	// }
  render() {
    return (
    	<div>
    			<div className='sidebar'>
    				<i className="fa fa-bars bar" onClick={this.props.toogleSidebar} ></i><br/>
					<ul>
						<div>Logo</div>
						<li className='header'><b>General</b></li>
						<Link to='/'><li><label className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage' alt='No'/></label><button className = 'sideButton1'>HOME</button></li></Link>
						<Link to ='/register'><li><span className='iconBorder'><img src="https://png.icons8.com/office/40/000000/user.png" className='iconImage' alt='No'/></span><button className = 'sideButton1'>REGISTER</button></li></Link>
						<Link to='/logs'><li><span className='iconBorder'><img src="https://png.icons8.com/color/40/000000/overview-pages-3.png" className='iconImage'alt='No'/></span><button className = 'sideButton1'>LOGS</button></li></Link>
						<Link to='/control'><li><span className='iconBorder'><img src="https://png.icons8.com/nolan/40/000000/robot-2.png" className='iconImage'alt='No'/></span><button className = 'sideButton1'>Controls</button></li></Link>
						<li><span className='iconBorder'><img src="https://png.icons8.com/dotty/40/000000/connection-status-on.png" className='iconImage'alt='No'/></span><button className = 'sideButton1'>Status</button></li>
						<li><span className='iconBorder'><img src="https://png.icons8.com/color/50/000000/home.png" className='iconImage'alt='No'/></span><button className = 'sideButton1'>LAYOUTS</button></li>
					</ul>
				</div>
    	</div>
    )
  }
}

export default Sidebars