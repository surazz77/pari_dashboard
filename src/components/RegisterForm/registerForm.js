import React from 'react'
import '../../App.css'
const RegisterForm=(props)=>(
	<div>
		<div className='register-form'>
            <div className='registerCard'>
                <h1>Register User</h1>
                <div className="card">
                <div className="container">
                    <form action='#'>
                    <label>First Name</label>
                    <input type="text" name="firstname" placeholder="Your name.."/>
                    <label>Last Name</label>
                    <input type="text" name="lastname" placeholder="Your last name.."/>

                    <label>Photo</label>
                    <input type="file" size="60" multiple />
                    <input type="submit" value="Submit"/>
                    
                    </form>
                </div>
                </div>
			</div>
		</div>
	</div>
	)
export default RegisterForm