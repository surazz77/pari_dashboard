import React, { Component } from 'react';
import Path from './Router/route'
class App extends Component {
  render() {
    return (
      <div className="App">
      	<Path/>
      </div>
    );
  }
}

export default App;
