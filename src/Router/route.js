import React from 'react'
import { BrowserRouter as Router, Route} from "react-router-dom";
import Home from '../components/Home/home'
import Log from '../components/Logs/logs'
import Register from '../components/Register/register'
import Control from '../components/Control/control'



const Path = (props) => (
		<Router>
			<div>
				<Route path="/" exact component={Home} />
				<Route path="/register" component={Register}/>
				<Route path="/logs" component={Log}/>
				<Route path="/control" component={Control}/>
			</div>
		</Router>

	)
export default Path